#!/usr/bin/env python
#looter.py

#this takes a webiste and crawls its links,
#saving them as HTML files for offline use
import urllib2
import os
import argparse
import random
import urlparse
from bs4 import BeautifulSoup

class Looter():
    def __init__(self,firstPage, numPages):

        self.firstPage = str(firstPage)
        self.numPages = int(numPages)
        print '\ninitializing LOOTER'

    def run(self):
        print 'running...'
        print 'source : {s}'.format(s=self.firstPage)
        
        #open the source file and save it
        if not os.path.exists('./downloaded/'):
            os.mkdir('downloaded')

        source = urllib2.urlopen(self.firstPage)
        prefix = self.firstPage
        contents = source.read()
        filename = self.firstPage.replace('/','')
        with open('./downloaded/'+filename,'w') as file:
            file.write(contents)
    
        #find all <a> tags and get their href
        #so a random link can be picked
        #do this for every subsequent page
        for page in range(0, self.numPages-1):
            try:
                links = []
                atags = BeautifulSoup(contents).find_all('a')
                for atag in atags:
                    links.append(atag.get('href'))

                link = random.choice(links)
                link = urlparse.urljoin(prefix, str(link))
                print '{number} : {name}'.format(number = page+2, name = link)
                filename = link.replace('/','')
                contents = urllib2.urlopen(link).read()

                with open('./downloaded/'+filename,'w') as file:
                    try:
                        file.write(contents)
                    except Exception as e:
                        print e

            except Exception as weberror:
                print 'ISSUE DOWNLOADING {u}'.format(u = link)
        print '\ndone'

if __name__ == '__main__':
    print 'running as main'
    parser = argparse.ArgumentParser()
    parser.add_argument('--sourceURL','-s', help='The URL you want to start crawling at')
    parser.add_argument('--numPages','-n', help='Number of links to grab.')
    args = vars(parser.parse_args())
    sourceURL = args['sourceURL']
    numPages = args['numPages']
    if sourceURL is None:
        sourceURL = 'http://www.en.wikipedia.org'
    if numPages is None:
        numPages = 20
    looter = Looter(sourceURL, numPages)
    looter.run()
