#   GUI FOR LOOTER
import sys
from PyQt4 import QtGui, QtCore
from looter import Looter

class LooterGui(QtGui.QWidget):
    def __init__(self):
        super(LooterGui, self).__init__()
        self.initUI()

    def initUI(self):
        grid = QtGui.QGridLayout()
        grid.setColumnStretch(0,100)

        self.urlBox = QtGui.QLineEdit('http://www.en.wikipedia.org', self)
        self.pageNumBox = QtGui.QSpinBox(self)
        self.pageNumBox.setMaximum(100)
        self.pageNumBox.setValue(10)
        confirmBtn = QtGui.QPushButton('Confirm',self)
        confirmBtn.clicked.connect(lambda: self.loot
                (self.urlBox.text(),int(self.pageNumBox.value())))

        urlLabel = QtGui.QLabel('URL : ', self)
        pageNumLabel = QtGui.QLabel('Number of pages : ', self)

        grid.setSpacing(5)
        grid.addWidget(urlLabel, 0,0)
        grid.addWidget(pageNumLabel,0,1)
        grid.addWidget(self.urlBox, 1,0)
        grid.addWidget(self.pageNumBox,1,1)
        grid.addWidget(confirmBtn,1,2)

        self.setLayout(grid)
        self.setGeometry(100,100,500,75)
        self.setWindowTitle('Looter')
        self.show()

    def loot(self,url,numPages):
        url = str(url)
        numPages = int(numPages)
        if url.isspace() or url == "":
            url = 'http://www.en.wikipedia.org'
        try:
            print url
            print numPages
            Looter(url, numPages).run()
        except Exception as e:
            #replace with logging
            print e

def main():
    app = QtGui.QApplication(sys.argv)
    lootergui = LooterGui()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
