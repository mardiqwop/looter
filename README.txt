-------------------------------------------------------------------
LOOTER is an app that lets you store a little chunk of the web
locally so you'll always have something to read whenever the
internet goes down or you don't have access for any other reason.
-------------------------------------------------------------------
-------------------------------------------------------------------

To use looter from the command line, type:

./looter.py

This defaults to 20 random wikipedia articles. In order to specify the
url and the number of links to traverse, use it like this:
./looter.py -s <URL> -n <NUMBERPAGES>

So to rip 10 random links starting at wikipedia, you would type:

./looter.py -s http://www.en.wikipedia.org -n 10

--------------------------------------------------------------------

A GUI is also included that can be launched with:

python main.py

--------------------------------------------------------------------

DEPENDENCIES:

Make sure you have the following packages installed:
  BeautifulSoup4
  PyQt4 (only necessary for the GUI)

-------------------------------------------------------------------
by mardiqwop  ;)
